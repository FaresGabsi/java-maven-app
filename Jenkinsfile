pipeline {
    agent any
    tools {
        maven 'maven-3.9'
    }
    environment {
        //Nexus Configuration
        NEXUS_VERSION = "nexus3"
        NEXUS_PROTOCOL = "http"
        NEXUS_URL = "192.168.1.9:8081"
        NEXUS_REPOSITORY = "artifactRepo"
        NEXUS_CREDENTIAL_ID = "jenkins"
        SONAR_TOKEN = credentials('sonar_token')
    }
    stages {
        stage("build jar") {
            steps {
                script {
                    // List the contents of the target/surefire-reports directory for debugging
                    sh 'ls -la target/surefire-reports'
                    sh "mvn package"
                }
            }
        }
        stage('SonarQube analysis') {
            steps {
                script
                        {
                            withSonarQubeEnv(installationName: 'sonar') {
                                sh 'mvn sonar:sonar -Dsonar.token=${SONAR_TOKEN}'
                            }
                        }

            }
        }
        stage("Publish to Nexus Repository Manager") {
            steps {
                script {
                    nexusArtifactUploader(
                            nexusVersion: NEXUS_VERSION,
                            protocol: NEXUS_PROTOCOL,
                            nexusUrl: NEXUS_URL,
                            groupId: 'com.example',
                            version: '1.1.0-SNAPSHOT',
                            repository: NEXUS_REPOSITORY,
                            credentialsId: NEXUS_CREDENTIAL_ID,
                            artifacts: [
                                    [artifactId: 'java-maven-app', file: "target/java-maven-app-1.1.0-SNAPSHOT.jar", type: 'jar']
                            ]
                    );
                }
            }
        }
    }
    post {
        always {
            // Collect and publish JUnit test results
            junit '**/target/surefire-reports/*.xml'
            // Collect and publish JaCoCo code coverage results
            jacoco execPattern: '**/target/jacoco.exec'
        }
    }
}
