
import com.example.Application;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
public class ApplicationTests {

    @Test
    public void contextLoads() {
        // This test will ensure that the application context loads without any issues
    }

    @Test
    public void testGetStatus() {
        Application app = new Application();
        assertEquals("OK", app.getStatus(), "The status should be OK");
    }

    @Test
    public void testAdd() {
        Application app = new Application();
        assertEquals(5, app.add(2, 3), "2 + 3 should equal 5");
        assertEquals(-1, app.add(-2, 1), "-2 + 1 should equal -1");
    }

    @Test
    public void testIsValidUser() {
        Application app = new Application();
        assertTrue(app.isValidUser("JohnDoe"), "Username 'JohnDoe' should be valid");
        assertFalse(app.isValidUser(null), "Null username should be invalid");
        assertFalse(app.isValidUser(""), "Empty username should be invalid");
    }
}
